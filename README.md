# About avc-commons3-all

## Introduction

This Maven project with `packaging=pom` is 
used to build as modules
all projects from the **avc-commons3** universe.

The `.gitlab-ci` configuration first
clones all required projects.

Projects that are built include:

* [avc-base-parent](https://gitlab.com/avcompris/avc-base-parent/)
* [avc-base-testutil](https://gitlab.com/avcompris/avc-base-testutil/)
* [avc-base-testutil-ut](https://gitlab.com/avcompris/avc-base-testutil-ut/)
* [avc-commons-parent](https://gitlab.com/avcompris/avc-commons-parent/)
* [avc-commons-lang](https://gitlab.com/avcompris/avc-commons-lang/)
* [avc-commons-testutil](https://gitlab.com/avcompris/avc-commons-testutil/)
* [avc-binding-common](https://gitlab.com/avcompris/avc-binding-common/)
* [avc-binding-dom](https://gitlab.com/avcompris/avc-binding-dom/)
* [avc-binding-json](https://gitlab.com/avcompris/avc-binding-json/)
* [avc-binding-yaml](https://gitlab.com/avcompris/avc-binding-yaml/)
* [avc-domdumper](https://gitlab.com/avcompris/avc-domdumper/)
* [avc-commons3-parent](https://gitlab.com/avcompris/avc-commons3-parent/)
* [avc-commons3-types](https://gitlab.com/avcompris/avc-commons3-types/)
* [avc-commons3-yaml](https://gitlab.com/avcompris/avc-commons3-yaml/)
* [avc-commons3-databeans](https://gitlab.com/avcompris/avc-commons3-databeans/)
* [avc-project-tools](https://gitlab.com/avcompris/avc-project-tools/)
* [avc-commons3-all-ut](https://gitlab.com/avcompris/avc-commons3-all-ut/)

## Dependency diagram

![avc_commons3_projects](doc/diagrams/png/avc_commons3_projects.png)

## Architecture

![avc_commons3_architecture](doc/diagrams/png/avc_commons3_architecture.png)

1. Source code is hosted at GitLab;
2. GitLab CI/CD runs jobs to build SNAPSHOT artifacts;
3. Some custom runners may be invoked;
4. SNAPSHOT artifacts are deployed to OSSRH;
5. A custom web site hosts the generated Maven sites;
6. Release artifacts are deployed to OSSRH;
7. And pushed to Central.